﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class BillBasicModel
    {
        public long BillId { get; set; }
        public String BillNo { get; set; }
        public int RoomId { get; set; }
        public string CheckinDate { get; set; }  
        public decimal Deposit { get; set; }
        public int TravelAgencyId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerIDNo { get; set; }
        public int UpdatedByUserId { get; set; }
        public int PayTypeId { get; set; }
        public decimal ConsumeFee { get; set; }
        public string ConsumeItem { get; set; }
        public int StayTotalHours { get; set; }
        public decimal SinglePrice { get; set; }//房间单价
        public decimal MinFeeForHourRoom { get; set; }//钟点房最低消费
        public decimal PricePerHour { get; set; }//钟点房每小时费用
        public short IsHourRoom { get; set; } //是否是钟点房
        public string Comments { get; set; }
   
    }
}