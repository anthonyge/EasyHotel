﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMSOFT.EasyHotel.DAL.Models
{
    [PetaPoco.TableName("Room")]
    [PetaPoco.PrimaryKey("RoomId")]
   public class Room
    {
        public int RoomId { get; set; }
        public string RoomNo { get; set; }
        public int RoomTypeId { get; set; }
        public int SubHotelId { get; set; }
        public bool IsMending { get; set; }
        private DateTime _created = DateTime.Now;
        public DateTime Created { get { return _created; } set { _created = value; } }
        private DateTime _modified = DateTime.Now;
        public DateTime Modified { get { return _modified; } set { _modified = value; } }
    }
}
